from django.db import models


class AbstractTimeStamp(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Created At')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Updated At')

    class meta:
        abstruct = True


class BookCollection(AbstractTimeStamp):
    name = models.CharField(max_length=128)
    author = models.CharField(max_length=128)
    publisher = models.CharField(max_length=128)
    isbn = models.CharField(max_length=52)
    publish_year = models.IntegerField()
    category = models.CharField(max_length=52)
    price = models.IntegerField()
    description = models.TextField()
    file = models.FileField(upload_to="books")

    def __str__(self):
        return "{} author : {}".format(self.name, self.author)
